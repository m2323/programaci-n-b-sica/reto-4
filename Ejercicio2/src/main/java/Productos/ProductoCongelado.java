/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Productos;

/**
 *
 * @author jcebalus
 */
public class ProductoCongelado extends Producto{
    private float temperatura;

    public ProductoCongelado(String fecha_caducidad, int lote, float temperatura) {
        super(fecha_caducidad, lote);
        this.temperatura = temperatura;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }

    @Override
    public void ingrearNombreProducto(String nombre_producto) {
        super.setNombre_producto(nombre_producto);
    }

    @Override
    public void ingresarCantidadActual(int cantidad_prodcuto) {
        super.setCantidad_producto(cantidad_prodcuto);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("\nTemperatura Recomendada: ").append(temperatura);
        return sb.toString();
    }
      
}
