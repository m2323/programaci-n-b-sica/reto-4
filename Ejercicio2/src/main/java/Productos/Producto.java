/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Productos;

/**
 *
 * @author jcebalus
 */
public abstract class Producto {
    private String fecha_caducidad;
    private int lote;
    private String nombre_producto;
    private int cantidad_producto;
    
    public Producto(String fecha_caducidad, int lote) {
        this.fecha_caducidad = fecha_caducidad;
        this.lote = lote;
    }

    public String getFecha_caducidad() {
        return fecha_caducidad;
    }

    public void setFecha_caducidad(String fecha_caducidad) {
        this.fecha_caducidad = fecha_caducidad;
    }

    public int getLote() {
        return lote;
    }

    public void setLote(int lote) {
        this.lote = lote;
    }
    
        public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public int getCantidad_producto() {
        return cantidad_producto;
    }

    public void setCantidad_producto(int cantidad_producto) {
        this.cantidad_producto = cantidad_producto;
    }
        
    public abstract void ingrearNombreProducto(String nombre_producto);
    
    public abstract void ingresarCantidadActual(int cantidad_prodcuto);

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Nombre del producto: ").append(nombre_producto);
        sb.append("\nCantidad de producto: ").append(cantidad_producto);
        sb.append("\nFecha de caducidad: ").append(fecha_caducidad);
        sb.append("\nLote: ").append(lote);
        return sb.toString();
    }
    



}
