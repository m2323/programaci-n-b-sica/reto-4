package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author jcebalus
 */
public class Conectar {
    
    private Connection CONN;
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String USER = "jcebalus";
    private static final String PASSWORD = "C3b4ll05_123";
    private static final String URL = "jdbc:mysql://localhost:3306/biblioteca";

    public Conectar() {
        CONN = null;

    }
    
    public Connection getConnection(){
        try {
            Class.forName(DRIVER);
            CONN = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error al establecer la conexión con la base de datos", JOptionPane.ERROR_MESSAGE);
        }
        return CONN;
    }   
    
    public void close(){
        try {
            CONN.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error al cerrar la conexión con la base de datos", JOptionPane.ERROR_MESSAGE);
        }
    }
}
