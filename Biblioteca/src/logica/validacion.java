/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.Arrays;

/**
 *
 * @author jcebalus
 */
public class validacion {
    private final String usuario = "admin";
    private final char [] contraseña = {'a','d','m','i','n','1','2','3'};
    
    public boolean validacionAdmin(String usuario, char[] password){
        if(usuario.equals(this.usuario)){
            if (Arrays.equals(password, contraseña)){
                return true;
            }
        }
        return false;
    }
    
    public boolean validarContraseñas (char[] password, char[] confirm_password){
        return Arrays.equals(password, confirm_password);
    }
}
