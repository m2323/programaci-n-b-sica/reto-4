/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import conexion.Conectar;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author jcebalus
 */
public class Autor {
    
    private final String nombre;
    private final String nacionalidad;
    private final String fecha_nacimiento;
    
    private final String SQL_INSERT = "INSERT INTO autores (full_name, nacionality, date_birth) VALUES (?,?,?)";
    private PreparedStatement PS;
    private final Conectar CN;

    public Autor(String nombre, String nacionalidad, String fecha_nacimiento) {
        this.nombre = nombre;
        this.nacionalidad = nacionalidad;
        this.fecha_nacimiento = fecha_nacimiento;
        PS = null;
        CN = new Conectar();
    }
    
    public void registarAutor(){
        try {
            PS = CN.getConnection().prepareStatement(SQL_INSERT);
            PS.setString(1, this.nombre);
            PS.setString(2, this.nacionalidad);
            PS.setString(3, this.fecha_nacimiento);
            int result = PS.executeUpdate();
            if (result > 0) {
                JOptionPane.showMessageDialog(null, "Autor registrado con éxito.");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error al guardar registro en la base de datos", JOptionPane.ERROR_MESSAGE);
        } finally {
            PS = null;
            CN.close();
        }
    }
    
}
