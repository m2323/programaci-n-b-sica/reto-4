/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import conexion.Conectar;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jcebalus
 */
public class Socio {
    
    private String username;
    private String password;
    private String full_name;
    private String phone;
    private String address;
    private String email;
    
    private int borrowed_books = 0;
    private int penalized_days = 0;
    
    private final String SQL_INSERT = "INSERT INTO socios (username,password,full_name,phone,address,email) VALUES (?,?,?,?,?,?)";
    private final String SQL_SELECT = "SELECT * FROM socios";
    private PreparedStatement PS;
    private DefaultTableModel table;
    private ResultSet RS;
    private final Conectar CN;

    public Socio() {
        PS = null;  
        CN = new Conectar();
    }    
    
    

    public Socio(String username, String password, String full_name, String phone, String address, String email) {
        this.username = username;
        this.password = password;
        this.full_name = full_name;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.borrowed_books = 0;
        this.penalized_days = 0;
        PS = null;  
        CN = new Conectar();
    }
    
    private void setTitulos() {
        table = new DefaultTableModel();
        table.addColumn("id");
        table.addColumn("Nombre");
        table.addColumn("Usuario");
        table.addColumn("Correo electrónico");
        table.addColumn("Teléfono");
        table.addColumn("Dirección");
        table.addColumn("Libros Prestados");
        table.addColumn("Días Penalizados");
        
        //return table;
    }

    public void ingresarSocio() {
        try {
            PS = CN.getConnection().prepareStatement(SQL_INSERT);
            PS.setString(1, this.username);
            PS.setString(2, this.password);
            PS.setString(3, this.full_name);
            PS.setString(4, this.phone);
            PS.setString(5, this.address);
            PS.setString(6, this.email);
            int result = PS.executeUpdate();
            if (result > 0) {
                JOptionPane.showMessageDialog(null, "Socio registrado con éxito.");
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error al guardar registro en la base de datos", JOptionPane.ERROR_MESSAGE);
        } finally {
            PS = null;
            CN.close();
        }
    }
    
    public DefaultTableModel obtenerSocios(){
        try {
            setTitulos();
            PS = CN.getConnection().prepareStatement(SQL_SELECT);
            RS = PS.executeQuery();
            
            Object[] fila = new Object[8];
            while (RS.next()){
                fila[0] = RS.getInt(1);
                fila[1] = RS.getString(4);
                fila[2] = RS.getString(2);
                fila[3] = RS.getString(7);
                fila[4] = RS.getString(5);
                fila[5] = RS.getString(6);
                fila[6] = RS.getInt(8);
                fila[7] = RS.getInt(9);
                table.addRow(fila);
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error al obtener los datos");
        } finally {
            PS = null;
            RS = null;
            CN.close();
        }
        return table;
    }
    
    public void prestarLibro(){
        if(this.borrowed_books <= 3){
            this.borrowed_books += this.borrowed_books + 1;
        } else {
            JOptionPane.showMessageDialog(null, "Has alcnazado la cantidad máxima de libros que puedes prestar.");
        }
    }

}
