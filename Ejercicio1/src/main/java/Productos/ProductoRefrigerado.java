/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Productos;

/**
 *
 * @author jcebalus
 */
public class ProductoRefrigerado extends Producto{
    
    private int supervision_alimentaria;

    public ProductoRefrigerado(String fecha_caducidad, int lote, int supervision_alimentaria) {
        super(fecha_caducidad, lote);
        this.supervision_alimentaria = supervision_alimentaria;
    }

    public int getSupervision_alimentaria() {
        return supervision_alimentaria;
    }

    public void setSupervision_alimentaria(int supervision_alimentaria) {
        this.supervision_alimentaria = supervision_alimentaria;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("\nCódigo de supervisión alimentaria: ").append(supervision_alimentaria);
        return sb.toString();
    }

}
