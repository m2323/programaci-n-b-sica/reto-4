/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Productos;

/**
 *
 * @author jcebalus
 */
public class ProductoFesco extends Producto{
    
    private String fecha_envasado;
    private String pais;

    public ProductoFesco(String fecha_caducidad, int lote, String fecha_envasado, String pais) {
        super(fecha_caducidad, lote);
        this.fecha_envasado = fecha_envasado;
        this.pais = pais;
    }

    public String getFecha_envasado() {
        return fecha_envasado;
    }

    public void setFecha_envasado(String fecha_envasado) {
        this.fecha_envasado = fecha_envasado;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("\nFecha de envasado: ").append(fecha_envasado);
        sb.append("\nPaís: ").append(pais);
        return sb.toString();
    }

}
