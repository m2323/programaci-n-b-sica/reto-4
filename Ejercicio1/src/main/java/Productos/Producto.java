/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Productos;

/**
 *
 * @author jcebalus
 */
public class Producto {
    private String fecha_caducidad;
    private int lote;

    public Producto(String fecha_caducidad, int lote) {
        this.fecha_caducidad = fecha_caducidad;
        this.lote = lote;
    }

    public String getFecha_caducidad() {
        return fecha_caducidad;
    }

    public void setFecha_caducidad(String fecha_caducidad) {
        this.fecha_caducidad = fecha_caducidad;
    }

    public int getLote() {
        return lote;
    }

    public void setLote(int lote) {
        this.lote = lote;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Fecha de Caducidad: ").append(fecha_caducidad);
        sb.append("\nLote: ").append(lote);
        return sb.toString();
    }
    
    
}
